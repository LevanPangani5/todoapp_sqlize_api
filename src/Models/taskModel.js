const { DataTypes } = require("sequelize");
const { sequelize } = require("./db");
const User = require('./userModel');
module.exports=(sequelize,DataTypes)=>{
    const Task = sequelize.define('tasks',{
        title: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
          },
          description: {
            type: DataTypes.STRING,
          },
          isDone: {
            type: DataTypes.TINYINT,
            defaultValue: 0,
          },
    })
    return Task;
}