const {Sequelize ,DataTypes} = require('sequelize');
const dotenv = require('dotenv');

dotenv.config({path:'../.env'});
const sequelize=new Sequelize(  
    process.env.DB,
    process.env.USER,
    process.env.PWD,
    {
      host:process.env.HOST,
      dialect:'mysql',
      define:{
        timestamps:true
      }
    },
  )
  
  sequelize.authenticate()
.then(()=>{
  console.log('connected')
}).catch(err=>{console.log("error : "+ err)});

const db ={};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require('./userModel')(sequelize,DataTypes);
db.tasks = require('./taskModel')(sequelize,DataTypes);

db.sequelize.sync({force:false})
.then(()=>{console.log(' re-sync done')});

db.users.hasMany(db.tasks,{
foreignKey:'user_id'});

module.exports=db;

