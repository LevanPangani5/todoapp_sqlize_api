const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth')



  function isValidEmail(email) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }

  router.use((req,res,next)=>{
    const { email, password } = req.body;
        
        if (!email || !password || password.length < 6 || !isValidEmail(email)) {
            return res.status(400).json({
              error: "InvalidRequest",
              message: "Email field contains an invalid email, or the length of a password is less than 6 characters or eny of the fields're missing ",
            });
        }
        next();
  })
    
    router.post('/register',authController.register);

    router.post('/login' , authController.login);
    
    module.exports=()=>{return router;}